import java.util.Arrays;
import java.util.TreeSet;

public class TreeSetCreator {
    public TreeSet treeSetCreator(Integer[] objects) {
        TreeSet<Integer> treeSet = new TreeSet();
        Arrays.stream(objects).forEach(o -> treeSet.add(o));
        return treeSet;

    }


}
