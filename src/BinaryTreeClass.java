public class BinaryTreeClass {
    private int key = 10;

    public int getKey() {
        return key;
    }

    SecondLevelLeftNodeClass secondLevelLeftNodeClass;
    SecondLevelRightNodeClass secondLevelRightNodeClass;
    /*public BinaryTreeClass() {
        secondLevelLeftNodeClass = new SecondLevelLeftNodeClass();
        secondLevelRightNodeClass = new SecondLevelRightNodeClass();
    }

     */
    public class SecondLevelLeftNodeClass {
        private int key = 5;

        public int getKey() {
            return key;
        }

        ThirdLevelLeftNodeLeftClass thirdLevelLeftNodeLeftClass;
        /*public SecondLevelLeftNodeClass() {
            thirdLevelLeftNodeLeftClass = new ThirdLevelLeftNodeLeftClass();

        }

         */
        public class ThirdLevelLeftNodeLeftClass {
            private int key = 1;

            public int getKey() {
                return key;
            }

            FourthLevelLeftNodeRightClass fourthLevelLeftNodeRightClass;

            /*public ThirdLevelLeftNodeLeftClass() {
                 fourthLevelLeftNodeRightClass = new FourthLevelLeftNodeRightClass();
            }

             */

            public class FourthLevelLeftNodeRightClass {
                private int key = 4;

                public int getKey() {
                    return key;
                }
            }
        }

    }


    public class SecondLevelRightNodeClass {
        private int key = 45;

        public int getKey() {
            return key;
        }

        ThirdLevelRightNodeLeftClass thirdLevelRightNodeLeftClass;
        ThirdLevelRightNodeRightClass thirdLevelRightNodeRightClass;

        /*public SecondLevelRightNodeClass() {
            thirdLevelRightNodeLeftClass = new ThirdLevelRightNodeLeftClass();
            thirdLevelRightNodeRightClass = new ThirdLevelRightNodeRightClass();

        }

         */

        public class ThirdLevelRightNodeLeftClass {
            private int key = 20;

            public int getKey() {
                return key;
            }

            FourthLevelRightNodeLeftClass fourthLevelRightNodeLeftClass;
            FourthLevelRightNodeRightClass fourthLevelRightNodeRightClass;
            /*public ThirdLevelRightNodeLeftClass() {
                fourthLevelRightNodeLeftClass = new FourthLevelRightNodeLeftClass();
                fourthLevelRightNodeRightClass = new FourthLevelRightNodeRightClass();
            }

             */
            public class FourthLevelRightNodeLeftClass {
                private int key = 15;

                public int getKey() {
                    return key;
                }
            }
            public class FourthLevelRightNodeRightClass {
                private int key = 25;
                public int getKey() {
                    return key;
                }
            }

        }
        public class ThirdLevelRightNodeRightClass {
            private int key = 99;
            public int getKey() {
                return key;
            }
        }

    }
}
