import java.util.Arrays;
import java.util.TreeSet;

public class Solution {
    public static void main(String[] args) {
        BinaryTreeClass.SecondLevelRightNodeClass.ThirdLevelRightNodeLeftClass.FourthLevelRightNodeRightClass binaryTreeClass0 = new BinaryTreeClass().new SecondLevelRightNodeClass().new ThirdLevelRightNodeLeftClass().new FourthLevelRightNodeRightClass();
        BinaryTreeClass.SecondLevelRightNodeClass.ThirdLevelRightNodeLeftClass.FourthLevelRightNodeLeftClass binaryTreeClass1 = new BinaryTreeClass().new SecondLevelRightNodeClass().new ThirdLevelRightNodeLeftClass().new FourthLevelRightNodeLeftClass();
        BinaryTreeClass.SecondLevelRightNodeClass.ThirdLevelRightNodeLeftClass binaryTreeClass2 = new BinaryTreeClass().new SecondLevelRightNodeClass().new ThirdLevelRightNodeLeftClass();
        BinaryTreeClass.SecondLevelRightNodeClass.ThirdLevelRightNodeRightClass binaryTreeClass3 = new BinaryTreeClass().new SecondLevelRightNodeClass().new ThirdLevelRightNodeRightClass();
        BinaryTreeClass.SecondLevelRightNodeClass binaryTreeClass4 = new BinaryTreeClass().new SecondLevelRightNodeClass();
        BinaryTreeClass binaryTreeClass = new BinaryTreeClass();
        BinaryTreeClass.SecondLevelLeftNodeClass.ThirdLevelLeftNodeLeftClass.FourthLevelLeftNodeRightClass binaryTreeClass5 = new BinaryTreeClass().new SecondLevelLeftNodeClass().new ThirdLevelLeftNodeLeftClass().new FourthLevelLeftNodeRightClass();
        BinaryTreeClass.SecondLevelLeftNodeClass.ThirdLevelLeftNodeLeftClass binaryTreeClass6 = new BinaryTreeClass().new SecondLevelLeftNodeClass().new ThirdLevelLeftNodeLeftClass();
        BinaryTreeClass.SecondLevelLeftNodeClass binaryTreeClass7 = new BinaryTreeClass().new SecondLevelLeftNodeClass();
        Integer[] objects = new Integer[9];
        objects[0] = binaryTreeClass0.getKey();
        objects[1] = binaryTreeClass1.getKey();
        objects[2] = binaryTreeClass2.getKey();
        objects[3] = binaryTreeClass3.getKey();
        objects[4] = binaryTreeClass4.getKey();
        objects[5] = binaryTreeClass5.getKey();
        objects[6] = binaryTreeClass6.getKey();
        objects[7] = binaryTreeClass7.getKey();
        objects[8] = binaryTreeClass.getKey();
        TreeSet<Integer> treeSet = new TreeSet();
        Arrays.stream(objects).forEach(o -> treeSet.add(o));
        for (Integer integer : treeSet) {
            System.out.print(integer + " ");
        }


    }
}
